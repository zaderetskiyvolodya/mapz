﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class LevelInfo
    {
        public int number;
        public string name;
        public string description;

        public LevelInfo()
        {
            number = 1;
            name = "Becoming a hero";
            description = LoremIpsum(25);

        }

        public string setInfo()
        {
            return "This is level #" + number + "\n" + name + "\nDescription:\n" + description+"\n";
        }




        static string LoremIpsum(int numWords)
        {

            var words = new[]{"lorem", "ipsum", "dolor", "sit", "amet", "consectetuer",
           "adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod",
           "tincidunt", "ut", "laoreet", "dolore", "magna", "aliquam", "erat"};

            var rand = new Random();

            StringBuilder result = new StringBuilder();

                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { result.Append(" "); }
                        result.Append(words[rand.Next(words.Length)]);
                    }
                    result.Append(". ");
               

            return result.ToString();
        }
    }

    
}
