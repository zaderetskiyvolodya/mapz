﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Tree
    {
        public static int amount;
        int number;
        int height;
        int width;
        TreeType type;


        public Tree(int height, int width, TreeType type)
        {
            this.height = height;
            this.width = width;
            this.type = type;
            amount++;
            this.number = amount;
        }

        public string setTree()
        {
            return "This is tree #" + number + "\nName: "+ type.name + "\nColor: "+ type.color+ "\nSize: "+height.ToString()+" x " + width.ToString()+ "\n\n";
        }
    }
}
