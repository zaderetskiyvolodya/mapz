﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab6.Prototype
{
    class Soldier: Enemy
    {
        private EnemyType enemyType;

        Soldier()
        {

        }
        public Enemy cloneEnemy(int enemy)
        {
            return new Soldier();
        }
    }

    class TrainedSoldier : Enemy
    {
        private EnemyType enemyType;

        TrainedSoldier()
        {

        }
        public Enemy cloneEnemy(int enemy)
        {
            return new TrainedSoldier();
        }
    }

    class Monster : Enemy
    {
        private EnemyType enemyType;

        Monster()
        {

        }
        public Enemy cloneEnemy(int enemy)
        {
            return new Monster();
        }
    }
}
