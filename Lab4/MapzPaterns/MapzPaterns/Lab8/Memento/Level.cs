﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MapzPaterns
{
    class Level
    {
        private string _state;
        private int _points;

        public Level(string state, int points)
        {
            this._state = state;
            this._points = points;
            Console.WriteLine("Level: My initial state is: " + state + "\nPoints = " + _points.ToString() );
        }

        public void DoSomething()
        {
            Console.WriteLine("Level: I'm changing a state.");
            this._state = this.GenerateRandomString(30);
            Console.WriteLine($"Level: and my state has changed to: {_state}  Points = {_points}");
        }

        private string GenerateRandomString(int length = 10)
        {
            string allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = string.Empty;

            while (length > 0)
            {
                result += allowedSymbols[new Random().Next(0, allowedSymbols.Length)];

                Thread.Sleep(12);

                length--;
            }

            _points = new Random().Next(0, 1000);

            return result;
        }

        public IMemento Save()
        {
            return new ConcreteMemento(this._state , this._points);
        }

        public void Restore(IMemento memento)
        {
            if (!(memento is ConcreteMemento))
            {
                throw new Exception("Unknown memento class " + memento.ToString());
            }

            this._state = memento.GetState();
            Console.Write($"Level: My state has changed to: {_state}");
        }
    }
}

