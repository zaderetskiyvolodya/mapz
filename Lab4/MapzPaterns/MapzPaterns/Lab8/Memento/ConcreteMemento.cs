﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MapzPaterns
{
     class ConcreteMemento : IMemento
    {
        private string _state;
        private int _points;

        private DateTime _date;

        public ConcreteMemento(string state, int points)
        {
            this._state = state;
            this._points = points;
            this._date = DateTime.Now;
        }

       
        public string GetState()
        {
            return this._state;
        }

       
        public string GetName()
        {
            return $"{this._date} / ({this._state.Substring(0, 9)})...   Points = {this._points}";
        }

        public int GetPoints()
        {
            return _points;
        }

        public DateTime GetDate()
        {
            return this._date;
        }
    }
}