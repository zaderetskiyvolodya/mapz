﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MapzPaterns
{
    public interface IMemento
    {
        string GetName();

        int GetPoints();

        string GetState();

        DateTime GetDate();
    }
}