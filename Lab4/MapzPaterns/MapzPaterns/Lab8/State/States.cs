﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class ImprovedPlayer : State
    {
        public override void DoResearch()
        {
            Console.WriteLine("ImprovedPlayer handles request1.");
            Console.WriteLine("It will take 5 minutes to finish research");
        }

        public override void RefreshHealth()
        {
            Console.WriteLine("ImprovedPlayer handles request2.");
            Console.WriteLine("Your HP will be restored  in 14 minutes");
        }
    }

    class RegularPlayer : State
    {
        public override void DoResearch()
        {
            Console.WriteLine("RegularPlayer handles request1.");
            Console.WriteLine("It will take 8 minutes to finish research");
        }

        public override void RefreshHealth()
        {
            Console.WriteLine("RegularPlayer handles request2.");
            Console.WriteLine("Your HP will be restored  in 20 minutes");
        }
    }
}
