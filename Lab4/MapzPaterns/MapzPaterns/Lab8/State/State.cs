﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    abstract class State
    {
        protected Context _context;

        public void SetContext(Context context)
        {
            this._context = context;
        }

        public abstract void DoResearch();

        public abstract void RefreshHealth();
    }
}
