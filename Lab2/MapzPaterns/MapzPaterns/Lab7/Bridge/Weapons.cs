﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Minigun : Weapon
    {
        public Minigun()
        {
            name = "Minigun";
            weight = 12;
        }

        override public string  fire()
        {
            return "BBBBBBBBBBBBBBBang!!!";
        }
    }
    class Rifle : Weapon
    {
        public Rifle()
        {
            name = "Rifle";
            weight = 12;
        }

        override public string fire()
        {
            return "Bang Bang Bang Bang!!";
        }
    }
}
