﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Players
    {
        public List<Player> players;

        public Players()
        {
            players = new List<Player>();
        }

        public string addPlayers()
        {
            Minigun minigun = new Minigun();
            Rifle rifle = new Rifle();

            players.Add(new Punisher(minigun));
            players.Add(new Destroyer(rifle));
            players.Add(new Punisher(minigun));
            players.Add(new Destroyer(rifle));

            string result = "";

            foreach(Player pl in players)
            {
                result += pl.write();
            }
            return result;
        }
    }
}
