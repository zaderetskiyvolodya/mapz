﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class TreeType
    {
        public string name;
        public string color;
        public TreeType(string name, string color)
        {
            this.name = name;
            this.color = color;
        }
    }
}
