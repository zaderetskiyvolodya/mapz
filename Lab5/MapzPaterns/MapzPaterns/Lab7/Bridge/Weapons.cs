﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    [Serializable]
    class Minigun : Weapon
    {
        public Minigun()
        {
            name = "Minigun";
            weight = 12;
        }

        override public string  fire()
        {
            return "BBBBBBBBBBBBBBBang!!!";
        }
    }
    [Serializable]
    class Rifle : Weapon
    {
        public Rifle()
        {
            name = "Rifle";
            weight = 12;
        }

        override public string fire()
        {
            return "Bang Bang Bang Bang!!";
        }
    }
}
