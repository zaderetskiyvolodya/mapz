﻿using MapzPaterns.Lab7.Flyweight;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MapzPaterns
{
    class Program
    {
        static void Main(string[] args)
        {
            // Lab 7
            
              
            Forest forest = new Forest();
            Players pl = new Players();
            
            //Facade
            Console.WriteLine("Facade\n\n");
             Console.WriteLine(LevelLoader.loadLevel());
           
            //Flyweight
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Flyweight\n\n");
            Console.WriteLine(forest.showTrees());
           
            //Bridge
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Bridge\n\n");
            Console.WriteLine(pl.addPlayers());

            
            //End Lab 7


            //Lab 8


            //Memento
            
             Console.WriteLine("Memento\n\n");

            Level originator = new Level("There is some data.", 580);
            Caretaker caretaker = new Caretaker(originator);

            for (int i = 0; i < 3; i++)
            {
                caretaker.Backup();
                originator.DoSomething();
            }

            Console.WriteLine();
            caretaker.ShowHistory();

            Console.WriteLine("\nClient: Now, let's rollback!\n");
            caretaker.Undo();

            Console.WriteLine("\n\nClient: Once more!\n");
            caretaker.Undo();

            Console.WriteLine();


            //Command
            Console.WriteLine();
            Console.WriteLine("Command\n");

            Invoker invoker = new Invoker();
            Character receiver = new Character();
            invoker.SetOnStart(new HPAddPerk(receiver, 10));
            invoker.SetOnFinish(new SpeedAddPerk(receiver, 12));

            invoker.DoSomethingImportant();


            //State
            Console.WriteLine();
            Console.WriteLine("State\n");

            var context = new Context(new ImprovedPlayer());
            context.Request1();
            context.Request2();
            Console.WriteLine();
            context.TransitionTo(new RegularPlayer());
            context.Request1();
            context.Request2();



            //Serialization
            Console.WriteLine();
            Console.WriteLine("Serialization\n");

            Minigun minigun = new Minigun();
            Punisher person = new Punisher(minigun);
            Console.WriteLine("Object "+person.name +" created");

            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("player.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, person);

                Console.WriteLine("Object serialized");
            }

            using (FileStream fs = new FileStream("player.dat", FileMode.OpenOrCreate))
            {
                Punisher newPerson = (Punisher)formatter.Deserialize(fs);

                Console.WriteLine("Object deserialized");
                Console.WriteLine($"Name: {newPerson.name} --- Weapon: {newPerson.weapon.name}");
            }
            

            // Lab 9

            //Iterator
            
            Console.WriteLine();
            Console.WriteLine("Iterator\n");

            var collection = new LevelsCollection();

            Console.WriteLine("Standart game traversal:");

            collection.ShowList();
            collection.setExtended();

            Console.WriteLine("\nExtended game vesion traversal:");
            collection.ShowList();
            

            //Observer
            
            Console.WriteLine();
            Console.WriteLine("Observer\n");

            var publisher = new Publisher();
            var observerA = new ConcreteObserverA();
            publisher.Attach(observerA);

            var observerB = new ConcreteObserverB();
            publisher.Attach(observerB);

            publisher.SomeLogic();
            publisher.SomeLogic();

            publisher.Detach(observerB);

            publisher.SomeLogic();
            


            //Template method
            
            Console.WriteLine();
            Console.WriteLine("Template method\n");
            Console.WriteLine("Same client code can work with different subclasses:");

            Client.ClientCode(new EnemyPeople());

            Console.Write("\n");

            Console.WriteLine("Same client code can work with different subclasses:");
            Client.ClientCode(new Monster());
            

             //Strategy
            
            Console.WriteLine();
            Console.WriteLine("Strategy\n");

            var player1 = new Player1();

            Console.WriteLine(" StrategyWeapon is set to minigun.");
            player1.SetWeapon(new Minigun());
            player1.TakeAShot();

            Console.WriteLine();

            Console.WriteLine(" StrategyWeapon is set to rifle.");
            player1.SetWeapon(new Rifle());
            player1.TakeAShot();
        }
    }
}
