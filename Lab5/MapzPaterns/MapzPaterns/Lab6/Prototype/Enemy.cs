﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab6.Prototype
{
    enum EnemyType{ Soldier, TrainedSoldier, Monster }
    interface Enemy
    {
        public Enemy cloneEnemy(int enemy);
    }
}
