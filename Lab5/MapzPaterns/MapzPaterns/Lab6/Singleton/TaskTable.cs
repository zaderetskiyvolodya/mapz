﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab6.Singleton
{
    class TaskTable
    {
        private List<string> tasks;
        private TaskTable instance;

        private TaskTable()
        {

        }


        public TaskTable getInstance()
        {
            if (instance == null)
            {
                instance = new TaskTable();
            }
            return instance;
        }

    }
}
