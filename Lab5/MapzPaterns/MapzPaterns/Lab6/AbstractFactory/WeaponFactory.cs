﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab6.AbstractFactory
{
    interface WeaponFactory
    {
       public Weapon GenerateWeapon();
    }
}
