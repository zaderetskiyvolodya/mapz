﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class HPAddPerk : IPerk
    {
        private Character _receiver;

        private int _hp;


        public HPAddPerk(Character receiver, int hp)
        {
            this._receiver = receiver;
            this._hp = hp;
        }

        public void Execute()
        {
            Console.WriteLine($"HPAddPerk: Increasing HP on {_hp}");
            this._receiver.HPIncrease(this._hp);
        }
    }
}
