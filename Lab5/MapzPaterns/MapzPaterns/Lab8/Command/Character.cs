﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Character
    {
        public void HPIncrease(int a)
        {
            Console.WriteLine($"Character: HP Increased on {a}");
        }

        public void SpeedIncrease(int b)
        {
            Console.WriteLine($"Character: Speed Increased on {b}");
        }
    }
}
