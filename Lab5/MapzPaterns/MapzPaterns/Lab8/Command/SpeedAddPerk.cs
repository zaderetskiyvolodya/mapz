﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns 
{
    class SpeedAddPerk : IPerk
    {
        private Character _receiver;

        private int _speed;


        public SpeedAddPerk(Character receiver, int speed)
        {
            this._receiver = receiver;
            this._speed = speed;
        }

        public void Execute()
        {
            Console.WriteLine($"SpeedAddPerk: Increasing HP on {_speed}");
            this._receiver.SpeedIncrease(this._speed);
        }
    }
}
