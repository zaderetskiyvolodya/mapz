﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Invoker
    {
        private IPerk _onStart;

        private IPerk _onFinish;

        public void SetOnStart(IPerk command)
        {
            this._onStart = command;
        }

        public void SetOnFinish(IPerk command)
        {
            this._onFinish = command;
        }


        public void DoSomethingImportant()
        {
            Console.WriteLine("Invoker: Increase HP on 10 points!");
            if (this._onStart is IPerk)
            {
                this._onStart.Execute();
            }

            Console.WriteLine("Invoker: Increase speed on 12 points!");
            if (this._onFinish is IPerk)
            {
                this._onFinish.Execute();
            }
        }
    }
}
