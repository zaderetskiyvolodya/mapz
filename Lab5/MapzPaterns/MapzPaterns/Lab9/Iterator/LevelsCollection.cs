﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace MapzPaterns
{
    class LevelsCollection : IteratorAggregate
    {
        List<string> _collection = new List<string>();

        bool extended = false;

        public LevelsCollection()
        {
            _collection.Add("First");
            _collection.Add("Second");
            _collection.Add("SecondExtended");
            _collection.Add("Third");

        }

        public void setExtended()
        {
            extended = !extended;
        }

        public List<string> getItems()
        {
            return _collection;
        }

        public void AddItem(string item)
        {
            this._collection.Add(item);
        }

        public void ShowList()
        {
            if (!extended)
            {
                foreach (var element in _collection)
                {
                    if (element.ToString() != "SecondExtended")
                    {
                        Console.WriteLine(element);
                    }
                }
            }
            else
            {
                foreach (var element in _collection)
                {
                    Console.WriteLine(element);
                }
            }
        }

        public override IEnumerator GetEnumerator()
        {
            return new AlphabeticalOrderIterator(this, extended);
        }
    }
}
