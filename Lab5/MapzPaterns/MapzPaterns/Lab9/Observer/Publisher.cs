﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace MapzPaterns
{
    public interface IPublisher
    {

        void Attach(IObserver observer);

        void Detach(IObserver observer);

        void Notify();
    }

    public class Publisher : IPublisher
    {

        public int State { get; set; } = -0;

        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            Console.WriteLine("Publisher: Attached an observer.");
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
            Console.WriteLine("Publisher: Detached an observer.");
        }

        public void Notify()
        {
            Console.WriteLine("Publisher: Notifying observers...");

            foreach (var observer in _observers)
            {
                observer.Update(this);
            }
        }

        public void SomeLogic()
        {
            Console.WriteLine("\nPublisher: I'm doing something important.");
            this.State = new Random().Next(0, 10);

            Thread.Sleep(15);

            Console.WriteLine("Publisher: My state has just changed to: " + this.State);
            this.Notify();
        }
    }
}
