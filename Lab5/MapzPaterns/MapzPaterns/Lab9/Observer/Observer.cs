﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    public interface IObserver
    {
        void Update(IPublisher subject);
    }

    class ConcreteObserverA : IObserver
    {
        public void Update(IPublisher subject)
        {
            if ((subject as Publisher).State < 3)
            {
                Console.WriteLine("ObserverA: Reacted to the event.");
            }
        }
    }

    class ConcreteObserverB : IObserver
    {
        public void Update(IPublisher subject)
        {
            if ((subject as Publisher).State == 0 || (subject as Publisher).State >= 2)
            {
                Console.WriteLine("ObserverB: Reacted to the event.");
            }
        }
    }
}
