﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Player1
    {

        private Weapon _weapon;

        public Player1()
        { }

        public Player1(Weapon weapon)
        {
            this._weapon = weapon;
        }

        public void SetWeapon(Weapon weapon)
        {
            this._weapon = weapon;
        }


        public void TakeAShot()
        {
            Console.WriteLine("Player1: Shooting!!");
            var result = this._weapon.fire();

            Console.WriteLine(result);
        }
    }
}
