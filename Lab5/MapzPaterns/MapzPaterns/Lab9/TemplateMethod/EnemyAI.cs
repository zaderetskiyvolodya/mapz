﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    abstract class EnemyAI
    {
        public void TemplateMethod()
        {
            this.Move();
            this.Upgrade();
            this.Attack();
            this.BuildSmth();
            this.Reheal();
        }

        protected void Move()
        {
            Console.WriteLine("EnemyAI says: I am moving");
        }

        protected void Attack()
        {
            Console.WriteLine("EnemyAI says: Prepare for the attack");
        }

        protected abstract void Upgrade();

        protected abstract void Reheal();

        protected virtual void BuildSmth() { }

    }

}
