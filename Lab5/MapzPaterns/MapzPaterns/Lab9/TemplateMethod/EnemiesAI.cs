﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class EnemyPeople : EnemyAI
    {
        protected override void Upgrade()
        {
            Console.WriteLine("EnemyPeople says: I'm upgrading as a person");
        }

        protected override void Reheal()
        {
            Console.WriteLine("EnemyPeople says: I'm rehealing faster than monsters");
        }

        protected override void BuildSmth()
        {
            Console.WriteLine("EnemyPeople says: I'm bulding the house");
        }
    }

    class Monster : EnemyAI
    {
        protected override void Upgrade()
        {
            Console.WriteLine("Monster says: I'm upgrading as a monster");
        }

        protected override void Reheal()
        {
            Console.WriteLine("Monster says: I'm rehealing slower than the people");
        }
    }
    class Client
    {
        public static void ClientCode(EnemyAI abstractClass)
        {
            
            abstractClass.TemplateMethod();
           
        }
    }
}
