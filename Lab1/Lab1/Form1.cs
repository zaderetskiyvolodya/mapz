﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        private static Token EOF = new Token(TokenType.EOF, "");

        private List<Token> tokens;
        private int pos, size;
        private int parens = 0;
        private bool lose,win;


        private int dirX, dirY;
        private int _width = 780;
        private int _height = 360;
        private int _sizeOfSides = 60;
        private List<PictureBox> water = new List<PictureBox>();
        private List<PictureBox> earth = new List<PictureBox>();

        public Form1()
        {
            InitializeComponent();
            this.Width = _width + 18;
            this.Height = 500;
            _generateMap();
            // this.KeyDown += new KeyEventHandler(OKP);
        }

        private void OKP(object sender, TokenType e)
        {
            switch (e)
            {
                case TokenType.RIGHT:
                    player.Location = new Point(player.Location.X + _sizeOfSides, player.Location.Y);
                    Task.Delay(500).Wait();
                    break;
                case TokenType.LEFT:
                    player.Location = new Point(player.Location.X - _sizeOfSides, player.Location.Y);
                    Task.Delay(500).Wait();
                    break;
                case TokenType.UP:
                    player.Location = new Point(player.Location.X, player.Location.Y - _sizeOfSides);
                    Task.Delay(500).Wait();
                    break;
                case TokenType.DOWN:
                    player.Location = new Point(player.Location.X, player.Location.Y + _sizeOfSides);
                    Task.Delay(500).Wait();
                    break;
            }
        }

        private void runBtn_Click(object sender, EventArgs e)
        {
            if (txt.Text == "")
            {
                MessageBox.Show("Write commands");
            }
            else
            {
                lose = false;
                win = false;
                player.Location = new Point(1, 1);
                List<Token> tokens = new Lexer(txt.Text).tokenize();
                parse(tokens, 0, tokens.Count);
            }
           
        }


        private void parse(List<Token> tokens, int first,int last) {
            for (int i = first; i < last; i++)
            {
                if (lose || win) break;

                if (tokens[i].Type == TokenType.RIGHT)
                {
                    if (i < tokens.Count - 1)
                    {
                        if ((tokens[i + 1].Type != TokenType.MULTIPLY))
                        {
                            OKP(player, TokenType.RIGHT);
                        }
                        else if ((tokens[i + 1].Type == TokenType.MULTIPLY) && (tokens[i + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[i + 2].Text); j++)
                            {
                                OKP(player, tokens[i].Type);
                                foreach (PictureBox pic in water)
                                {
                                    if (player.Location == pic.Location)
                                    {
                                        MessageBox.Show("You lose!!");
                                        lose = true;
                                        break;
                                    }
                                }
                                if (lose || win) break;
                            }
                            i = i + 3;
                        }
                    }
                    else
                    {
                        OKP(player, TokenType.RIGHT);
                    }
                    foreach (PictureBox pic in water)
                    {
                        if (player.Location == pic.Location)
                        {
                            MessageBox.Show("You lose!!");
                            lose = true;
                            break;
                        }
                        if (player.Location == new Point(721,301))
                        {
                            MessageBox.Show("You win!!");
                            win = true;
                            break;
                        }
                    }
                    if (lose || win) break;
                    continue;
                }
                if (tokens[i].Type == TokenType.LEFT)
                {
                    if (i < tokens.Count - 1)
                    {
                        if ((tokens[i + 1].Type != TokenType.MULTIPLY))
                        {
                            OKP(player, TokenType.LEFT);
                        }
                        else if ((tokens[i + 1].Type == TokenType.MULTIPLY) && (tokens[i + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[i + 2].Text); j++)
                            {
                                OKP(player, tokens[i].Type);
                                foreach (PictureBox pic in water)
                                {
                                    if (player.Location == pic.Location)
                                    {
                                        MessageBox.Show("You lose!!");
                                        lose = true;
                                        break;
                                    }
                                }
                                if (lose || win) break;
                            }
                            i = i + 3;
                        }
                    }
                    else
                    {
                        OKP(player, TokenType.LEFT);
                    }
                    foreach (PictureBox pic in water)
                    {
                        if (player.Location == pic.Location)
                        {
                            MessageBox.Show("You lose!!");
                            lose = true;
                            break;
                        }
                        if (player.Location == new Point(721, 301))
                        {
                            MessageBox.Show("You win!!");
                            win = true;
                            break;
                        }
                    }
                    if (lose || win) break;
                    continue;
                }
                if (tokens[i].Type == TokenType.UP)
                {
                    if (i < tokens.Count - 1)
                    {
                        if ((tokens[i + 1].Type != TokenType.MULTIPLY))
                        {
                            OKP(player, TokenType.UP);
                        }
                        else if ((tokens[i + 1].Type == TokenType.MULTIPLY) && (tokens[i + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[i + 2].Text); j++)
                            {
                                OKP(player, tokens[i].Type);
                                foreach (PictureBox pic in water)
                                {
                                    if (player.Location == pic.Location)
                                    {
                                        MessageBox.Show("You lose!!");
                                        lose = true;
                                        break;
                                    }
                                }
                                if (lose || win) break;
                            }
                            i = i + 3;
                        }
                    }
                    else
                    {
                        OKP(player, TokenType.UP);
                    }
                    foreach (PictureBox pic in water)
                    {
                        if (player.Location == pic.Location)
                        {
                            MessageBox.Show("You lose!!");
                            lose = true;
                            break;
                        }
                        if (player.Location == new Point(721, 301))
                        {
                            MessageBox.Show("You win!!");
                            win = true;
                            break;
                        }
                    }
                    if (lose || win) break;
                    continue;
                }
                if (tokens[i].Type == TokenType.DOWN)
                {
                    if (i < tokens.Count - 1)
                    {
                        if ((tokens[i + 1].Type != TokenType.MULTIPLY))
                        {
                            OKP(player, TokenType.DOWN);
                        }
                        else if ((tokens[i + 1].Type == TokenType.MULTIPLY) && (tokens[i + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[i + 2].Text); j++)
                            {
                                OKP(player, tokens[i].Type);
                                foreach (PictureBox pic in water)
                                {
                                    if (player.Location == pic.Location)
                                    {
                                        MessageBox.Show("You lose!!");
                                        lose = true;
                                        break;
                                    }
                                }
                                if (lose || win) break;
                            }
                            i = i + 3;
                        }
                    }
                    else
                    {
                        OKP(player, TokenType.DOWN);
                    }   
                    foreach (PictureBox pic in water)
                    {
                        if (player.Location == pic.Location)
                        {
                            MessageBox.Show("You lose!!");
                            lose = true;
                            break;
                        }
                        if (player.Location == new Point(721, 301))
                        {
                            MessageBox.Show("You win!!");
                            win = true;
                            break;
                        }
                    }
                    if (lose || win) break;
                    continue;
                }
                if (tokens[i].Type == TokenType.PLUS)
                {
                    continue;
                }
                if (tokens[i].Type == TokenType.NUMBER)
                {
                    
                    if (tokens[i + 1].Type != TokenType.MULTIPLY)
                    {
                        continue;
                    }
                     else if (tokens[i + 2].Type == TokenType.LPAREN)
                    {
                        
                        int lst = i + 3;
                        while(tokens[lst].Type != TokenType.RPAREN)
                        {
                            lst++;
                        }
                        for(int j = 0; j<Double.Parse(tokens[i].Text); j++)
                        {
                            parse(tokens, i + 3, lst+1);
                            if (lose || win) break;
                        }
                        i = lst + 1;
                    }
                    else
                    {
                        
                        for (int j = 0; j < Double.Parse(tokens[i].Text) - 1; j++)
                        {
                            OKP(player, tokens[i+2].Type);
                            foreach (PictureBox pic in water)
                            {
                                if (player.Location == pic.Location)
                                {
                                    MessageBox.Show("You lose!!");
                                    lose = true;
                                    break;
                                }
                            }
                            if (lose || win) break;
                        }
                    }
                }

                if (tokens[i].Type == TokenType.MULTIPLY)
                {
                    
                    continue;
                }

                if (tokens[i].Type == TokenType.LPAREN)
                {
                    if (i == 0)
                    {
                        int lst = i;
                        while (tokens[lst].Type != TokenType.RPAREN)
                        {
                            lst++;
                        }
                        if((tokens[lst+1].Type == TokenType.MULTIPLY) && (tokens[lst + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[lst+2].Text); j++)
                            {
                                parse(tokens, i + 1, lst );
                                if (lose || win) break;
                            }
                            i = lst + 1;
                        }
                    }else if(tokens[i-1].Type == TokenType.PLUS)
                    {
                        int lst = i;
                        while (tokens[lst].Type != TokenType.RPAREN)
                        {
                            lst++;
                        }
                        if ((tokens[lst + 1].Type == TokenType.MULTIPLY) && (tokens[lst + 2].Type == TokenType.NUMBER))
                        {
                            for (int j = 0; j < Double.Parse(tokens[lst + 2].Text); j++)
                            {
                                parse(tokens, i + 1, lst);
                                if (lose || win) break;
                            }
                            i = lst + 1;
                        }
                    }
                    continue;
                }
            }
        }

        private void _generateMap()
        {
            for (int i = 0; i <= _height / _sizeOfSides; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.Black;
                pic.Location = new Point(0, _sizeOfSides * i);
                pic.Size = new Size(_width, 1);
                this.Controls.Add(pic);
            }
            for (int i = 0; i <= _width / _sizeOfSides; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.Black;
                pic.Location = new Point(_sizeOfSides * i, 0);
                pic.Size = new Size(1, _height);
                this.Controls.Add(pic);
            }

            for (int j = 0; j < 4; j++)
            {
                for (int i = j; i < j + 2; i++)
                {
                    PictureBox pic = new PictureBox();
                    pic.BackColor = Color.DarkGreen;
                    pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * j + 1);
                    pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                    this.Controls.Add(pic);
                }
            }

            for (int i = 5; i < 11; i++)
            {
                if (i == 9) continue;
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.DarkGreen;
                pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * 3 + 1);
                pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                this.Controls.Add(pic);
            }

            for (int i = 8; i < 11; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.DarkGreen;
                pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * 4 + 1);
                pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                this.Controls.Add(pic);
            }

            for (int i = 10; i < 13; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.DarkGreen;
                pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * 2 + 1);
                pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                this.Controls.Add(pic);
            }

            for (int i = 3; i < 6; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.DarkGreen;
                pic.Location = new Point(12 * _sizeOfSides + 1, _sizeOfSides * i + 1);
                pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                this.Controls.Add(pic);
                if (i == 5) earth.Add(pic);
            }

            for (int j = 0; j < 4; j++)
            {
                for (int i = j + 2; i < 13; i++)
                {
                    if (j == 2)
                    {
                        if (i > 9) continue;
                    }
                    if (j == 3)
                    {
                        if (i < 9 || i == 10 || i == 12) continue;
                    }
                    PictureBox pic = new PictureBox();
                    pic.BackColor = Color.Blue;
                    pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * j + 1);
                    pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                    this.Controls.Add(pic);
                    water.Add(pic);
                }
            }

            for (int j = 1; j < 6; j++)
            {
                for (int i = 0; i < 12; i++)
                {
                    if (j == 1 && i > 0)
                    {
                        break;
                    }
                    if (j == 2 && i > 1)
                    {
                        break;
                    }
                    if (j == 3 && i > 2)
                    {
                        break;
                    }
                    if (j == 4 && i > 7 && i < 11)
                    {
                        continue;
                    }
                    PictureBox pic = new PictureBox();
                    pic.BackColor = Color.Blue;
                    pic.Location = new Point(i * _sizeOfSides + 1, _sizeOfSides * j + 1);
                    pic.Size = new Size(_sizeOfSides - 1, _sizeOfSides - 1);
                    this.Controls.Add(pic);
                    water.Add(pic);

                }
            }
        }
        /*
        private Expression additive()
        {
            Expression result = multiply();
            while (true)
            {
                if (match(TokenType.PLUS))
                {
                    result = new BinaryExpresion('+', result, multiply());
                    continue;
                }
                if (match(TokenType.MINUS))
                {
                    result = new BinaryExpresion('-', result, multiply());
                    continue;
                }
                break;
            }
            return result;
        }

        private Expression multiply()
        {
            Expression result = unary();
            while (true)
            {
                if (match(TokenType.MULTIPLY))
                {
                    result = new BinaryExpresion('*', result, unary());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression unary()
        {
            if (match(TokenType.MINUS))
            {
                return new UnaryExpression('-', primary());
            }
            if (match(TokenType.PLUS))
            {
                return primary();
            }
            return primary();
        }
        private Expression primary()
        {
            Token current = get(0);
            if (match(TokenType.NUMBER))
            {
                return new ValueExpression(Double.Parse(current.Text));
            }
            if (get(0).Type == TokenType.WORD && get(1).Type == TokenType.LPAREN)
            {
                return function();
            }
            if (match(TokenType.WORD))
            {
                return new VariableExpression(current.Text);
            }
            if (match(TokenType.TEXT))
            {
                return new ValueExpression(current.Text);
            }
            if (match(TokenType.LPAREN))
            {
                Expression result = expression();
                if (match(TokenType.RPAREN))
                {
                    return result;
                }
            }

            throw new Exception("Unknown expresion");
        }*/

        private Token consume(TokenType type)
        {
            Token current = get(0);
            if (type != current.Type) throw new Exception("Token " + current + " doesn't match " + type);
            pos++;
            return current;
        }

        private bool match(TokenType type)
        {
            Token current = get(0);
            if (type != current.Type) return false;
            pos++;
            return true;
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {

        }

        private Token get(int relativePosition)
        {
            int position = pos + relativePosition;
            if (position >= size) return EOF;
            return tokens[position];


        }

        private async Task waitAsync()
        {
            await Task.Delay(10000);
        }
    }
}
