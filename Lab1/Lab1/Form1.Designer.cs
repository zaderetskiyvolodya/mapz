﻿namespace Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.player = new System.Windows.Forms.PictureBox();
            this.txt = new System.Windows.Forms.TextBox();
            this.runBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            this.SuspendLayout();
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Yellow;
            this.player.Image = global::Lab1.Properties.Resources.icons8_futurama_bender_48;
            this.player.Location = new System.Drawing.Point(1, 1);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(59, 59);
            this.player.TabIndex = 0;
            this.player.TabStop = false;
            // 
            // txt
            // 
            this.txt.Location = new System.Drawing.Point(5, 375);
            this.txt.Multiline = true;
            this.txt.Name = "txt";
            this.txt.Size = new System.Drawing.Size(600, 80);
            this.txt.TabIndex = 1;
            this.txt.Text = "3*(Right+Down)+Right*5+Down+2*Right+2*Up+2*Right+3*Down";
            this.txt.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // runBtn
            // 
            this.runBtn.Location = new System.Drawing.Point(636, 406);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(127, 23);
            this.runBtn.TabIndex = 2;
            this.runBtn.Text = "Run";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.txt);
            this.Controls.Add(this.player);
            this.Name = "Form1";
            this.Text = "Help Bender Find Exit";
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.TextBox txt;
        private System.Windows.Forms.Button runBtn;
    }
}

