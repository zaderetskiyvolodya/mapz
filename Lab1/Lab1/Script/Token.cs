﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Token
    {
        private TokenType type;
        private string text;


        public Token()
        {
        }
        public Token(TokenType type, string text)
        {
            this.type = type;
            this.text = text;
        }

        public override string ToString()
        {
            return type.ToString() + " " + text;
        }

        public TokenType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
    }
}
