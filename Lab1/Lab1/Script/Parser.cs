﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Parser
    {
    
      /*  private static Token EOF = new Token(TokenType.EOF, "");

        private List<Token> tokens;
        private int pos, size;
        private int parens = 0;

        public Parser(List<Token> tokens)
        {
            this.tokens = tokens;
            size = tokens.Count;
        }

        private Statement statement()
        {
            if (match(TokenType.PRINT))
            {
                return new PrintStatement(expression());
            }
            if (match(TokenType.IF))
            {
                return ifElse();
            }
            if (match(TokenType.WHILE))
            {
                return WhileStatement();
            }
            if (match(TokenType.FOR))
            {
                return forStatement();
            }
            if (match(TokenType.BREAK))
            {
                return new BreakStatement();
            }
            if (match(TokenType.CONTINUE))
            {
                return new ContinueStatement();
            }
            if (match(TokenType.RETURN))
            {
                return new ReturnStatement(expression());
            }
            if (match(TokenType.DEF))
            {
                return functionDefine();
            }
            if (get(0).Type == TokenType.WORD && get(1).Type == TokenType.LPAREN)
            {
                return new FunctionalStatement(function());
            }
            return assignmentStatement();
        }

        private Statement assignmentStatement()
        {
            Token current = get(0);
            if (current.Type == TokenType.WORD && get(1).Type == TokenType.EQ)
            {
                match(TokenType.WORD);
                string variable = current.Text;
                match(TokenType.EQ);
                return new AssignmentStatement(variable, expression());
            }
            throw new Exception("Unknown object");
        }

        private Statement ifElse()
        {
            Expression condition = expression();
            Statement ifStatement = statementOrBlock();
            Statement elseStatement;
            if (match(TokenType.ELSE))
            {
                elseStatement = statementOrBlock();
            }
            else
            {
                elseStatement = null;
            }
            return new IfStatement(condition, ifStatement, elseStatement);
        }

        public Statement WhileStatement()
        {
            Expression condition = expression();
            Statement statement = statementOrBlock();
            return new WhileStatement(condition, statement);
        }

        private Statement forStatement()
        {
            Statement initialization = assignmentStatement();
            consume(TokenType.COMMA);
            Expression termination = expression();
            consume(TokenType.COMMA);
            Statement increment = assignmentStatement();
            Statement block = statementOrBlock();
            return new ForStatement(initialization, termination, increment, block);
        }

        private FunctionalExpression function()
        {
            string name = consume(TokenType.WORD).Text;
            consume(TokenType.LPAREN);
            FunctionalExpression function = new FunctionalExpression(name);
            while (!match(TokenType.RPAREN))
            {
                function.addArguments(expression());
                match(TokenType.COMMA);
            }
            return function;
        }

        private FunctionDefStatement functionDefine()
        {
            string name = consume(TokenType.WORD).Text;
            consume(TokenType.LPAREN);
            List<string> argNames = new List<string>();
            while (!match(TokenType.RPAREN))
            {
                argNames.Add(consume(TokenType.WORD).Text);
                match(TokenType.COMMA);
            }
            Statement body = statementOrBlock();
            return new FunctionDefStatement(name, argNames, body);
        }

        private Expression expression()
        {
            return logicalOr();
        }

        private Expression logicalOr()
        {
            Expression result = logicalAnd();

            while (true)
            {
                if (match(TokenType.BARBAR))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.OR, result, logicalAnd());
                    continue;
                }
                break;
            }

            return result;
        }

        private Expression logicalAnd()
        {
            Expression result = equality();

            while (true)
            {
                if (match(TokenType.AMPAMP))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.AND, result, equality());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression equality()
        {
            Expression result = conditional();

            if (match(TokenType.EQEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.EQUALS, result, conditional());
            }
            if (match(TokenType.EXCLEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.NOT_EQUALS, result, conditional());
            }
            return result;
        }

        private Expression conditional()
        {
            Expression result = additive();
            while (true)
            {
                if (match(TokenType.LT))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LT, result, additive());
                    continue;
                }
                if (match(TokenType.LTEQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LTEQ, result, additive());
                    continue;
                }
                if (match(TokenType.GT))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GT, result, additive());
                    continue;
                }
                if (match(TokenType.GTEQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GTEQ, result, additive());
                    continue;
                }
                break;
            }
            return result;
        }

        private Expression additive()
        {
            Expression result = multiply();
            while (true)
            {
                if (match(TokenType.PLUS))
                {
                    result = new BinaryExpresion('+', result, multiply());
                    continue;
                }
                if (match(TokenType.MINUS))
                {
                    result = new BinaryExpresion('-', result, multiply());
                    continue;
                }
                break;
            }
            return result;
        }

        private Expression multiply()
        {
            Expression result = unary();
            while (true)
            {
                if (match(TokenType.MULTIPLY))
                {
                    result = new BinaryExpresion('*', result, unary());
                    continue;
                }
                if (match(TokenType.DIVIDE))
                {
                    result = new BinaryExpresion('/', result, unary());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression unary()
        {
            if (match(TokenType.MINUS))
            {
                return new UnaryExpression('-', primary());
            }
            if (match(TokenType.PLUS))
            {
                return primary();
            }
            return primary();
        }
        private Expression primary()
        {
            Token current = get(0);
            if (match(TokenType.NUMBER))
            {
                return new ValueExpression(Double.Parse(current.Text));
            }
            if (get(0).Type == TokenType.WORD && get(1).Type == TokenType.LPAREN)
            {
                return function();
            }
            if (match(TokenType.WORD))
            {
                return new VariableExpression(current.Text);
            }
            if (match(TokenType.TEXT))
            {
                return new ValueExpression(current.Text);
            }
            if (match(TokenType.LPAREN))
            {
                Expression result = expression();
                if (match(TokenType.RPAREN))
                {
                    return result;
                }
            }

            throw new Exception("Unknown expresion");
        }

        private Token consume(TokenType type)
        {
            Token current = get(0);
            if (type != current.Type) throw new Exception("Token " + current + " doesn't match " + type);
            pos++;
            return current;
        }

        private bool match(TokenType type)
        {
            Token current = get(0);
            if (type != current.Type) return false;
            pos++;
            return true;
        }

        private Token get(int relativePosition)
        {
            int position = pos + relativePosition;
            if (position >= size) return EOF;
            return tokens[position];
        }*/
    }
}
