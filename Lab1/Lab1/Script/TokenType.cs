﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    enum TokenType
    {
        NUMBER,
        WORD,

        RIGHT,
        LEFT,
        UP,
        DOWN,

        PLUS,
        MINUS,
        MULTIPLY,

        LPAREN,
        RPAREN,

        EOF
    }
}
