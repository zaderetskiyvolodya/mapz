﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Lexer
    {

        private string OPERATOR_CHARS = "+-*()";
        
        private static Dictionary<string, TokenType> OPERATORS;
        static Lexer()
        {
            OPERATORS = new Dictionary<string, TokenType>();
            OPERATORS.Add("+", TokenType.PLUS);
            OPERATORS.Add("-", TokenType.MINUS);
            OPERATORS.Add("*", TokenType.MULTIPLY);
            OPERATORS.Add("(", TokenType.LPAREN);
            OPERATORS.Add(")", TokenType.RPAREN);

        }
        private string input;
        private List<Token> tokens;

        private int pos, Length;

        public Lexer(string input)
        {
            this.input = input;
            Length = input.Length;

            tokens = new List<Token>();
        }

        public List<Token> tokenize()
        {
            while (pos < Length)
            {
                char current = peek(0);
                if (Char.IsDigit(current))
                {
                    tokenizeNumber();
                }
                else if (Char.IsLetter(current))
                {
                    tokenizeWord();
                }
                else if (OPERATOR_CHARS.IndexOf(current) != -1)
                {
                    tokenizeOperator();
                }
                else
                {
                    next();
                }
            }
            return tokens;
        }

        private void tokenizeWord()
        {
            StringBuilder buffer = new StringBuilder();
            char current = peek(0);
            while (true)
            {
                if (!Char.IsLetterOrDigit(current) && (current != '_') && (current != '$')) { break; }
                buffer.Append(current);
                current = next();
            }
            string word = buffer.ToString();
            switch (word)
            {
                case "Right": addToken(TokenType.RIGHT); break;
                case "Left": addToken(TokenType.LEFT); break;
                case "Up": addToken(TokenType.UP); break;
                case "Down": addToken(TokenType.DOWN); break;
                default:
                    addToken(TokenType.WORD, buffer.ToString());
                    break;
            }
        }

        private void tokenizeOperator()
        {
            char current = peek(0);
            StringBuilder buffer = new StringBuilder();
            while (true)
            {
                String text = buffer.ToString();
                if (!OPERATORS.ContainsKey(text + current) && !string.IsNullOrEmpty(text))
                {
                    addToken(OPERATORS[text]);
                    return;
                }
                buffer.Append(current);
                current = next();
            }
        }


        private void tokenizeNumber()
        {
            StringBuilder buffer = new StringBuilder();
            char current = peek(0);
            while (true)
            {
                if (current == ',')
                {
                    if (buffer.ToString().IndexOf(",") != -1) throw new ArgumentException("Invalid float number");
                }
                else if (!Char.IsDigit(current)) { break; }
                buffer.Append(current);
                current = next();
            }
            addToken(TokenType.NUMBER, buffer.ToString());
        }

        private char next()
        {
            pos++;
            return peek(0);
        }

        private char peek(int relativePosition)
        {
            int position = pos + relativePosition;
            if (position >= Length) return '\0';
            return input[position];
        }

        private void addToken(TokenType type)
        {
            addToken(type, "");
        }

        private void addToken(TokenType type, string text)
        {
            tokens.Add(new Token(type, text));
        }
    }
}
