﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns
{
    class Player
    {
        public Weapon weapon;
        public string name;
        public int power;

        public string write()
        {
            return "This is player " + name + " with weapon: " + weapon.name + "\nHe shoots with sound: " + weapon.fire() + "\n\n";
        }
    }
    class Punisher : Player
    {
        public Punisher(Weapon w)
        {
            name = "Punisher";
            weapon = w;
            power = 80;
        }
    }
    class Destroyer : Player
    {
        public Destroyer(Weapon w)
        {
            name = "Destroyer";
            weapon = w;
            power = 100;
        }
    }
}
