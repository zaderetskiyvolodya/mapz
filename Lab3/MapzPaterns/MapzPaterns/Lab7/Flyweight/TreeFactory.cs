﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab7.Flyweight
{
    class TreeFactory
    {
        public List<TreeType> types ;

        public TreeFactory()
        {
             types = new List<TreeType>();
    }

        public void addTypes()
        {
            types.Add(new TreeType("Dub", "Dark Green"));
            types.Add(new TreeType("Bereza", "Brihgt Green"));
            types.Add(new TreeType("Sosna", "Green"));
        }
    }
}
