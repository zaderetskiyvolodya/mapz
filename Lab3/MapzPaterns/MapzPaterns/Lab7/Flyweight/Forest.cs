﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab7.Flyweight
{
    class Forest
    {
        public List<Tree> trees = new List<Tree>();

        public Forest()
        {
            addTrees(trees);

        }

        public void addTrees(List<Tree> trees)
        {
            Random rand = new Random();
            TreeFactory factory = new TreeFactory();
            factory.addTypes();
            for (int i = 0; i < 6; i++)
            {
                int randh = rand.Next(10, 50);
                int randw = rand.Next(1, 5);
                int randt = rand.Next(0,3);
                trees.Add(new Tree(randh, randw, factory.types[randt]));
            }
        }

        public string showTrees()
        {
            string result = "";
            foreach(Tree tree in trees)
            {
                result += tree.setTree();
            }
            return result;
        }
    }
}
