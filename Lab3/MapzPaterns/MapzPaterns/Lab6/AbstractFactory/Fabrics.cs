﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapzPaterns.Lab6.AbstractFactory
{
    class MeeleWeapon : WeaponFactory
    {
        public Weapon GenerateWeapon()
        {
            return new Sword();
        }
    }

    class LongRangeWeapon : WeaponFactory
    {
        public Weapon GenerateWeapon()
        {
            return new Gun();
        }
    }
}
