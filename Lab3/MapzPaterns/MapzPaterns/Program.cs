﻿using MapzPaterns.Lab7.Flyweight;
using System;

namespace MapzPaterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Forest forest = new Forest();
            Players pl = new Players();
            
            //Facade
            Console.WriteLine("Facade\n\n");
             Console.WriteLine(LevelLoader.loadLevel());
           
            //Flyweight
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Flyweight\n\n");
            Console.WriteLine(forest.showTrees());
           
            //Bridge
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Bridge\n\n");
            Console.WriteLine(pl.addPlayers());
        }
    }
}
